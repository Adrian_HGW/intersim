# InterSim

# Background

The frequency of studies on intersectionality are increasing in public health research and pursue the identification of health inequalities. Due to methodological complexity, several best practice approaches have been proposed recently. Some consider models specified with all $1^{st}$ and $2^{nd}$ order effects and another uses multi-variable stratification.

# Issues

Some of the proposed methods impose high model complexity which is known to generalize poorly in independent data when being too complex. For example, considering all interaction terms adds $2^p - p - 1$ parameter to a model, i.e. complexity increases exponentially with higher number of covariates. None of these approaches penalizes the complexity of the model in any way. 

In the presence of continuous covariates finding the optimal set of covariates and the best functional form is still an open issue (please see: [Sauerbrei et al. State of the art in selection of variables and functional forms in multivariable analysis—outstanding issues. Diagnostic and Prognostic Research. 2020;4(1):3](https://diagnprognres.biomedcentral.com/articles/10.1186/s41512-020-00074-3)).

Arbitrary categorization of continuous covariates is known to be harmful in terms of efficacy, accuracy, the assumption of piece-wise constant trajectories of data, and more. Nevertheless, it appears standard in some approaches of examining intersectionality

# Content

This project hosts 2 files for the primary analysis:

- 1-Simulation-Parallel.R"                                                                    
- 2-Summary-of-simulation-results-main.R"           

two files for sensitivity analyses

- 4-Simulation-Parallel-Covariance.R"                                                         
- 5-Summary-of-simulation-results-suppl.R"                                                    

and four files for plotting, illustration, and utility functions

- 3-Association-plots.R"                                                                      
- Z-01-utility-functions.R"                                                                   
- Z-02-SNR.R"                                                                                 
- Z-03-associations-structure-DEAS.R"    

# Literature

## Intersectionality

- Potter et al.. Intersectionality and Smoking Cessation: Exploring Various Approaches for Understanding Health Inequities. Nicotine & Tobacco Research. 2020;23(1):115-23.
- Wemrell et al.. An intersectional analysis providing more precise information on inequities in self-rated health. International Journal for Equity in Health. 2021;20(1):54.
- Warner LR. A Best Practices Guide to Intersectional Approaches in Psychological Research. Sex Roles. 2008;59(5):454-63.
- Mahendran M, Lizotte D, Bauer GR. Describing Intersectional Health Outcomes: An Evaluation of Data Analysis Methods. Epidemiology. 2022;33(3).
- Merlo J. Multilevel analysis of individual heterogeneity and discriminatory accuracy (MAIHDA) within an intersectional framework. Social Science & Medicine. 2018;203:74-80

## Don't you dear to dichotomize or to categorize:

- Altman DG. Categorising continuous variables. Br J Cancer. 1991;64(5):975.
- Buettner et al.. Problems in defining cutoff points of continuous prognostic factors: Example of tumor thickness in primary cutaneous melanoma. Journal of Clinical Epidemiology. 1997;50(11):1201-10.
- Faraggi D, Simon R. A Simulation Study Of A Cross-Validation For Selecting An Optimal Cutpoint In Univariate Survival Analysis. Statistics in Medicine. 1996;15(20):2203-13.
- Lausen B, Schumacher M. Evaluating the effect of optimized cutoff values in the assessment of prognostic factors. Computational Statistics & Data Analysis. 1996;21(3):307-26.
- Ragland DR. Dichotomizing Continuous Outcome Variables: Dependence of the Magnitude of Association and Statistical Power on the Cutpoint. Epidemiology. 1992;3(5):434-40.
- Royston P, Altman DG, Sauerbrei W. Dichotomizing continuous predictors in multiple regression: a bad idea. Statistics in Medicine. 2006;25(1):127-41.
- Harrell FE, Jr. Regression Modeling Strategies: With Applications, to Linear Models, Logistic and Ordinal Regression, and Survival Analysis, 2nd ed.: Springer Cham Heidelberg New York Dordtrecht London; 2015
- Thoresen M. Spurious interaction as a result of categorization. BMC Medical Research Methodology. 2019;19(1):28. 10.1186/s12874-019-0667-2.

