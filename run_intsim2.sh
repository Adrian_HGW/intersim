#!/bin/bash
#SBATCH -J int_sim2
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --partition=pinky
#SBATCH -t 71:59:59


## load modules
module load r/4.3.1

## run script
Rscript "4-Simulation-Parallel-Covariance.R"

