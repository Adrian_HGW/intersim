# Prediction over Association ----------------------------------------------------------------------
library(arules)
library(MASS)
library(splines)
library(foreach)
library(doParallel)
library(ggplot2)
library(data.table)
library(ggh4x)

# functions ----------------------------------------------------------------------------------------
util_prep_df <- function(x) {
  
  # to dataframe
  .x <- as.data.frame(x)
  
  # assign variable names
  names(.x) <- c("Age", "BMI", "Sex", "Education", "Diabetes", "Y1")
  
  # round continuous data to integer
  .x$Age <- round(.x$Age, digits = 0)
  .x$BMI <- round(.x$BMI, digits = 1)
  .x$Y1 <- round(.x$Y1, digits = 2)
  
  # create one variable for sex
  .x$Sex <- ifelse(.x$Sex < median(.x$Sex), 0, 1)
  
  # create one variable for education
  .x$Education <- arules::discretize(.x$Education,
                                     method = "fixed",
                                     breaks = c(-Inf, 
                                                as.numeric(quantile(.x$Education, 
                                                                    c(0.2, 0.45, 0.8))),
                                                Inf),
                                     labels = FALSE)
  
  .x$Education <- as.numeric(.x$Education)
  
  # create one variable for Diabetes
  .x$Diabetes <- ifelse(.x$Diabetes < quantile(.x$Diabetes, 0.85), 0, 1)
  
  # Y2:Y4 with non-linear association
  agesc <- as.numeric(scale(.x$Age, scale = FALSE))
  .x$Y2 <- 5 + 0.05 * agesc + 0.3*ifelse(.x$Sex == 0, 1, 0) + 0.008 * 
    ifelse(agesc > 0 & .x$Sex == 0, 1, 0) * agesc^2 + rnorm(Ns, 0, 1.25)
  .x$Y3 <- 5 + 0.05 * agesc + 0.3*ifelse(.x$Sex == 0, 1, 0) + 0.008 * 
    ifelse(agesc > 0 & .x$Sex == 0, 1, 0) * agesc^2 + rnorm(Ns, 0, 0.85)
  .x$Y4 <- 5 + 0.05 * agesc + 0.3*ifelse(.x$Sex == 0, 1, 0) + 0.008 * 
    ifelse(agesc > 0 & .x$Sex == 0, 1, 0) * agesc^2 + rnorm(Ns, 0, 0.18)
  
  return(x = .x)
  
}


util_to_factors <- function(x) {
  
  .x <- x
  
  # sex
  .x$Sex <- factor(.x$Sex, 
                   levels = c(0, 1), 
                   labels = c("females", "males"))
  
  # education
  .x$Education <- factor(.x$Education, 
                         levels = c(1:4), 
                         labels = c("none", "low", "average", "high"))
  
  # Diabetes
  .x$Diabetes <- factor(.x$Diabetes, 
                        levels = c(0, 1), 
                        labels = c("no", "yes"))
  
  # Age group
  # a heuristic approach for categorization of age into groups
  .x$AgeG <- arules::discretize(.x$Age, 
                                method = "fixed", 
                                breaks = c(-Inf, 48, 55, 62, Inf))
  
  # BMI group
  # a heuristic approach for categorization of age into groups
  .x$BMIG <- arules::discretize(.x$BMI, 
                                method = "fixed", 
                                breaks = c(-Inf, 18.5, 30, Inf))
  
  # stratification variable according to Potter et al. & Wemrell et al.
  .x$Strata_ASE <- paste0(.x$AgeG, " | ", .x$Sex, " | ", .x$Education)
  
  # set reference levels
  .x$Sex <- relevel(.x$Sex, ref = "males")
  .x$Strata_ASE <- relevel(factor(.x$Strata_ASE), 
                           ref = paste0(levels(.x$AgeG)[2], " | males | average"))
  
  return(x = .x)
  
}

# Settings -----------------------------------------------------------------------------------------
# sample size
NNs <- c(200, 300, 500, 1000, 2000, 3000)

# number of variables
nvar <- 6

# responses
rsps <- paste0("Y", 1:4)
rsps <- paste0("Y", c(1, 3))

# Covariance matrix
sigma1 <- matrix(rep(0, nvar^2), nrow = nvar)
diag(sigma1) <- c(100, 25, 1, 1, 1, 2)

# vector of means
means <- c(55, 25, rep(0, nvar - 3), 5)

# number of samples
nsamp <- 1000

# split to equally seized sampling frames
hm <- 10
nsampl <- split(1:nsamp, sort(1:nsamp%%hm))


# Repeat model fitting in identical setting, but save predictions in iVD ---------------------------

st <- Sys.time()

cl <- parallel::makeCluster(10)
clusterSetRNGStream(cl, 11235)
doParallel::registerDoParallel(cl)

st1 <- foreach(i = 1:length(nsampl),
              .combine = rbind,
              .packages = c("MASS", "splines", "arules")) %dopar% {

  for (Ns in NNs) {

  .r <- list()
    
    # 1000 samples     
    for (j in nsampl[[i]]) {
  
      set.seed(j)
      
      z1 <- mvrnorm(Ns, mu = means, Sigma = sigma1, empirical = TRUE)
      
      # create dataframe 
      ds1 <- util_prep_df(z1)
      
      # categorizes to discrete predictors, create strata, and reference levels
      ds1 <- util_to_factors(ds1)
      
      # define learnings data and validation data, bootstrap (oversampled)
      bsi <- sample(1:Ns, Ns, replace = TRUE)
      lds <- ds1[bsi, ]
      vds <- ds1[!(1:Ns %in% bsi), ]
      
      # predictions in validation data
      .rj <- vds
      .rj$N <- Ns
      .rj$I <- j
      
      for (r in rsps) {
        
        .rjr <- .rj
        
        # which response
        .rjr$R <- r
        
        ## full model including two-way interactions (discrete)
        lm1 <- lm(lds[[r]] ~ (AgeG + BMIG + Sex + Education + Diabetes)^2, data = lds)
        # predictions in validation data
        .rjr$P1 <- predict(lm1, newdata = vds, type = "response")
  
        ## full model including two-way interactions (mixed)
        lm2 <- lm(lds[[r]] ~ (Age + BMI + Sex + Education + Diabetes)^2, data = lds)
        # predictions in validation data
        .rjr$P2 <- predict(lm2, newdata = vds, type = "response")
  
        ## Strata: Age | Sex | Education (ASE)
        lm3 <- lm(lds[[r]] ~ Strata_ASE, data = lds)
        # predictions in validation data
        .rjr$P3 <- try(predict(lm3, newdata = vds, type = "response"), silent = TRUE)
  
        ## Stepwise BIC
        b0 <- lm(lds[[r]] ~ (Age + BMI + Sex + Education + Diabetes)^2, data = lds)
        lm4 <- step(b0, direction = 'backward', k = log(Ns), trace = FALSE)
        # predictions in validation data
        .rjr$P4 <- predict(lm4, newdata = vds, type = "response")
  
        # AGE + BMI non-linear (df=2)
        ## Stepwise BIC
        b1 <- lm(lds[[r]] ~ (ns(Age, df = 2) + ns(BMI, df = 2) + Sex + Education + Diabetes)^2, 
                 data = lds)
        lm5 <- step(b1, direction = 'backward', k = log(Ns), trace = FALSE)
        # predictions in validation data
        .rjr$P5 <- predict(lm5, newdata = vds, type = "response")
  
        # AGE + BMI non-linear (df=3)
        ## Stepwise BIC
        b2 <- lm(lds[[r]] ~ (ns(Age, df = 3) + ns(BMI, df = 3) + Sex + Education + Diabetes)^2, 
                 data = lds)
        lm6 <- step(b2, direction = 'backward', k = log(Ns), trace = FALSE)
        # predictions in validation data
        .rjr$P6 <- predict(lm6, newdata = vds, type = "response")
  
        # AGE + BMI non-linear (df=4)
        ## Stepwise BIC
        b3 <- lm(lds[[r]] ~ (ns(Age, df = 4) + ns(BMI, df = 4) + Sex + Education + Diabetes)^2, 
                 data = lds)
        lm7 <- step(b3, direction = 'backward', k = log(Ns), trace = FALSE)
        # predictions in validation data
        .rjr$P7 <- predict(lm7, newdata = vds, type = "response")
        
        if(r == "Y1") {
          .rjrdf <- .rjr
        } else {
          .rjrdf <- rbind(.rjrdf, .rjr)
        }
  
      }
      
      # collect
      .r[[j]] <- .rjrdf
     
    }
  
    # collect results
    .rdf <- do.call("rbind", .r)
    
    if (Ns == 200) {
      rdf <- .rdf
    } else {
      rdf <- rbind(rdf, .rdf)
    }  
  
  }

  # return results
  rdf
  
}


stopCluster(cl)

et <- Sys.time()
et - st # 

# save 
saveRDS(st1, file = "T:/Projekte/Intersectionality Simulation Study/data/model-predictions.RDS")

# Plot predicted values ----------------------------------------------------------------------------
mp <- readRDS("T:/Projekte/Intersectionality Simulation Study/data/model-predictions.RDS")
mp <- mp[mp$Age %in% 30:80 & mp$N %in% c(200, 2000),]
# remove failed predictions of approach 3
mp$P3 <- as.numeric(mp$P3)

# add true effect
agesc <- as.numeric(scale(mp$Age, scale = FALSE))
mp$TE <- 5 + 0.05 * agesc + 0.3*ifelse(mp$Sex == "females", 1, 0) + 
  0.008 * ifelse(agesc > 0 & mp$Sex == "females", 1, 0) * agesc^2
mp$TE[mp$R == "Y1"] <- 5

# to long format
mpl <- melt(setDT(mp),
            measure.vars = paste0("P", 1:7),
            id.vars = c("Age", "Sex", "N", "R", "TE"),
            variable.name = "Approach",
            value.name = "Predicted")

# into df
mpl <- as.data.frame(mpl)
mpl <- mpl[mpl$Approach %in% c("P1", "P3", "P6"), ]
mpl$Approach <- factor(mpl$Approach, levels = c("P1", "P3", "P6"), labels = c("Approach 1a",
                                                                              "Approach 2",
                                                                              "Approach 3c"))

mpl$N <- factor(mpl$N, levels = c(200, 2000), labels = c("N = 200", "N = 2000"))
mpl$R <- factor(mpl$R, levels = c("Y1", "Y3"), labels = c("SNR = 0", "SNR = 1"))
# age -> discrete scale to allow overlay plot of true effect
mpl$Age <- factor(mpl$Age, levels = 30:80, labels = 30:80)

ggplot(mpl, aes(x = Age, y = Predicted, color = Sex)) + 
  geom_boxplot(position = "dodge", outlier.color = NA, linewidth = 0.5) + 
  coord_cartesian(ylim = c(2.5, 10)) +
  scale_x_discrete(breaks = seq(30, 80, 5)) +
  geom_line(data = mpl, aes(x = as.numeric(mpl$Age), y = TE, linetype = Sex, color = Sex), 
            linewidth = 1.2) + 
  scale_fill_manual(values = c('#d8b365','#5ab4ac')) +
  scale_color_manual(values = c('#d8b365','#5ab4ac')) +
  ylab("Predictions of fitted models in internal validation data (iVD)") +
  facet_nested(Approach + N ~ R) +
  theme_minimal(base_size = 14) +
  theme(legend.position = c(0.25, 0.1),
        legend.title = element_blank())

ggsave(last_plot(),
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/fX-predictions-in-iVD.png",
       dpi = 600,
       width = 9,
       height = 12)

# into df
mpl <- as.data.frame(mpl)
mpl <- mpl[mpl$Approach %in% c("P2", "P4", "P5", "P7"), ]
mpl$Approach <- factor(mpl$Approach, levels = c("P2", "P4", "P5", "P7"), 
                       labels = c("Approach 1b",
                                  "Approach 3a",
                                  "Approach 3b",
                                  "Approach 3d"))

mpl$N <- factor(mpl$N, levels = c(200, 2000), labels = c("N = 200", "N = 2000"))
mpl$R <- factor(mpl$R, levels = c("Y1", "Y3"), labels = c("SNR = 0", "SNR = 1"))
# age -> discrete scale to allow overlay plot of true effect
mpl$Age <- factor(mpl$Age, levels = 30:80, labels = 30:80)

ggplot(mpl, aes(x = Age, y = Predicted, color = Sex)) + 
  geom_boxplot(position = "dodge", outlier.color = NA, linewidth = 0.5) + 
  coord_cartesian(ylim = c(2.5, 10)) +
  scale_x_discrete(breaks = seq(30, 80, 5)) +
  geom_line(data = mpl, aes(x = as.numeric(mpl$Age), y = TE, linetype = Sex, color = Sex), 
            linewidth = 1.2) + 
  scale_fill_manual(values = c('#d8b365','#5ab4ac')) +
  scale_color_manual(values = c('#d8b365','#5ab4ac')) +
  ylab("Predictions of fitted models in internal validation data (iVD)") +
  facet_nested(Approach + N ~ R) +
  theme_minimal(base_size = 14) +
  theme(legend.position = c(0.25, 0.1),
        legend.title = element_blank())

ggsave(last_plot(),
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/fX-suppl-predictions-in-iVD.png",
       dpi = 600,
       width = 9,
       height = 14)

# EOF ----------------------------------------------------------------------------------------------
