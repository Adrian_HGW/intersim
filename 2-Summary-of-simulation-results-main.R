# Prediction over Association ----------------------------------------------------------------------
library(MASS)
library(data.table)
library(dplyr)
library(ggplot2)
library(ggsci)
library(ggpubr)
library(corrplot)
library(GGally)
library(stringr)
library(geomtextpath)
library(cowplot)
library(splines)

# data of simulation results -----------------------------------------------------------------------

rdf_bw <- readRDS("T:/Projekte/Intersectionality Simulation Study/data/rdf-2023-09-22.RDS")

# Frequency of correct model and number of selected variables in setting 1 #########################

# select data for response with no association
y1df <- rdf_bw[rdf_bw$R == "Y1", ]

cvars <- paste0("lm", 4:7, "_C")
nvars <- paste0("lm", 4:7, "_N")

for (i in 1:4) {
  y1df[[nvars[i]]] <- gsub(" \\| ", " ", y1df[[cvars[i]]])
  y1df[[nvars[i]]] <- lengths(strsplit(y1df[[nvars[i]]], ' ')) - 1 # coefficients without intercept
}

# how often is intercept model correctly found?
vtrue <- paste0("lm", 1:7, "_true")
nvars <- paste0("lm", 1:7, "_N")

for (i in 1:7) {
  y1df[[vtrue[i]]] <- factor(ifelse(y1df[[nvars[i]]] == 0, 1, 0), 
                             levels = c(0, 1), 
                             labels = c("no", "yes"))
}

# initialize summary table
st <- table(y1df$lm1_true, y1df$S)

# bind remaining tables
for (i in 2:7) {
  st <- rbind(st, table(y1df[[vtrue[i]]], y1df$S))
}

# prep table
rn <- rownames(st)
st <- as.data.frame(st)
rownames(st) <- NULL

names(st) <- paste0("N = ", names(st))

st$Correct <- rn
st$Model <- rep(c("Approach 1a",
                  "Approach 1b",
                  "Approach 2",
                  "Approach 3a",
                  "Approach 3b",
                  "Approach 3c",
                  "Approach 3d"), each = 2)

st <- st[st$Correct != "no", ]
st$Correct <- NULL
st <- st[, c(7, 1:6)]

st[2:7] <- apply(st[2:7], 2, function(x){sprintf("%1.2f%%", x/10)})
st

# openxlsx::write.xlsx(st,
#                      file = "T:/Projekte/Intersectionality Simulation Study/GandT/Frequency-Intercept-Only-Models.xlsx")
 

# Frequency if incorrectly selected terms ----------------------------------------------------------
# 1st and 2nd order effects (discrete) -------------------------------------------------------------

t1 <- aggregate(y1df$lm1_N, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t1 <- cbind(t1[, 1], as.data.frame(t1$x))
t1$Q <- paste0("(", t1$`25%`, "; ", t1$`50%`, "; ", t1$`75%`, ")")


# 1st and 2nd order effects (mixed) ----------------------------------------------------------------

t2 <- aggregate(y1df$lm2_N, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t2 <- cbind(t2[, 1], as.data.frame(t2$x))
t2$Q <- paste0("(", t2$`25%`, "; ", t2$`50%`, "; ", t2$`75%`, ")")

# 1st and 2nd order effects (mixed) ----------------------------------------------------------------

t3 <- aggregate(y1df$lm3_N, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t3 <- cbind(t3[, 1], as.data.frame(t3$x))
t3$Q <- paste0("(", t3$`25%`, "; ", t3$`50%`, "; ", t3$`75%`, ")")

# BIC (linear) -------------------------------------------------------------------------------------
y1df$lm4_C <- gsub("\\(Intercept\\) \\| |\\(Intercept\\)", "", y1df$lm4_C)
y1df$lm4_C <- gsub("low|average|high", "", y1df$lm4_C)
y1df$lm4_C <- gsub("Sexfemales", "Sex", y1df$lm4_C)
y1df$lm4_C <- gsub("Diabetesyes", "Diabetes", y1df$lm4_C)
y1df$lm4_C <- gsub(" \\| ", " ", y1df$lm4_C)
y1df$lm4_C[y1df$lm4_C == ""] <- NA

# how many times correct?
y1df$lm4_FALSE <- unlist(lapply(y1df$lm4_C, function(x){
  ifelse(is.na(x), 0, length(unlist(str_split(x, " "))))
}))

t4 <- aggregate(y1df$lm4_FALSE, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t4 <- cbind(t4[, 1], as.data.frame(t4$x))
t4$Q <- paste0("(", t4$`25%`, "; ", t4$`50%`, "; ", t4$`75%`, ")")

# BIC (Age + BMI with 2df) -------------------------------------------------------------------------

y1df$lm5_C <- gsub("\\(Intercept\\) \\| |\\(Intercept\\)", "", y1df$lm5_C)
y1df$lm5_C <- gsub("low|average|high", "", y1df$lm5_C)
y1df$lm5_C <- gsub("Sexfemales", "Sex", y1df$lm5_C)
y1df$lm5_C <- gsub("Diabetesyes", "Diabetes", y1df$lm5_C)
y1df$lm5_C <- gsub(" \\| ", " ", y1df$lm5_C)
y1df$lm5_C <- gsub("ns\\(Age, df = 2\\)1|ns\\(Age, df = 2\\)2", "Age", y1df$lm5_C)
y1df$lm5_C <- gsub("ns\\(BMI, df = 2\\)1|ns\\(BMI, df = 2\\)2", "BMI", y1df$lm5_C)
y1df$lm5_C[y1df$lm5_C == ""] <- NA

y1df$lm5_FALSE <- unlist(lapply(y1df$lm5_C, function(x){
  ifelse(is.na(x), 0, length(unlist(str_split(x, " "))))
}))

t5 <- aggregate(y1df$lm5_FALSE, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t5 <- cbind(t5[, 1], as.data.frame(t5$x))
t5$Q <- paste0("(", t5$`25%`, "; ", t5$`50%`, "; ", t5$`75%`, ")")

# BIC (Age + BMI with 3df) -------------------------------------------------------------------------

y1df$lm6_C <- gsub("\\(Intercept\\) \\| |\\(Intercept\\)", "", y1df$lm6_C)
y1df$lm6_C <- gsub("low|average|high", "", y1df$lm6_C)
y1df$lm6_C <- gsub("Sexfemales", "Sex", y1df$lm6_C)
y1df$lm6_C <- gsub("Diabetesyes", "Diabetes", y1df$lm6_C)
y1df$lm6_C <- gsub(" \\| ", " ", y1df$lm6_C)
y1df$lm6_C <- gsub("ns\\(Age, df = 3\\)1|ns\\(Age, df = 3\\)2|ns\\(Age, df = 3\\)3", "Age", y1df$lm6_C)
y1df$lm6_C <- gsub("ns\\(BMI, df = 3\\)1|ns\\(BMI, df = 3\\)2|ns\\(BMI, df = 3\\)3", "BMI", y1df$lm6_C)
y1df$lm6_C[y1df$lm6_C == ""] <- NA

y1df$lm6_FALSE <- unlist(lapply(y1df$lm6_C, function(x){
  ifelse(is.na(x), 0, length(unlist(str_split(x, " "))))
}))

t6 <- aggregate(y1df$lm6_FALSE, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t6 <- cbind(t6[, 1], as.data.frame(t6$x))
t6$Q <- paste0("(", t6$`25%`, "; ", t6$`50%`, "; ", t6$`75%`, ")")

# BIC (Age + BMI with 4df) -------------------------------------------------------------------------

y1df$lm7_C <- gsub("\\(Intercept\\) \\| |\\(Intercept\\)", "", y1df$lm7_C)
y1df$lm7_C <- gsub("low|average|high", "", y1df$lm7_C)
y1df$lm7_C <- gsub("Sexfemales", "Sex", y1df$lm7_C)
y1df$lm7_C <- gsub("Diabetesyes", "Diabetes", y1df$lm7_C)
y1df$lm7_C <- gsub(" \\| ", " ", y1df$lm7_C)
y1df$lm7_C <- gsub("ns\\(Age, df = 4\\)1|ns\\(Age, df = 4\\)2|ns\\(Age, df = 4\\)3|ns\\(Age, df = 4\\)4", "Age", y1df$lm7_C)
y1df$lm7_C <- gsub("ns\\(BMI, df = 4\\)1|ns\\(BMI, df = 4\\)2|ns\\(BMI, df = 4\\)3|ns\\(BMI, df = 4\\)4", "BMI", y1df$lm7_C)
y1df$lm7_C[y1df$lm7_C == ""] <- NA

y1df$lm7_FALSE <- unlist(lapply(y1df$lm7_C, function(x){
  ifelse(is.na(x), 0, length(unlist(str_split(x, " "))))
}))

t7 <- aggregate(y1df$lm7_FALSE, list(y1df$S), function(x) {x <- quantile(x, probs = c(0.25, 0.5, 0.75))})
t7 <- cbind(t7[, 1], as.data.frame(t7$x))
t7$Q <- paste0("(", t7$`25%`, "; ", t7$`50%`, "; ", t7$`75%`, ")")

# combine correct and incorrect
st1all <- as.data.frame(t(st))[2:7, ]
names(st1all) <- gsub("Stepwise ", "BE-", st$Model)
st1all$"Sample size" <- rownames(st1all)
rownames(st1all) <- NULL
st1all$`Sample size` <- gsub("N = ", "", st1all$`Sample size`)

tl <- list(t1, t2, t3, t4, t5, t6, t7)

for (i in 1:7) {
  st1all[, i] <- paste0(st1all[, i], " ", tl[[i]]$Q)
}

st1all <- st1all[, c(8, 1:7)]

# openxlsx::write.xlsx(st1all, 
#                      file = "T:/Projekte/Intersectionality Simulation Study/GandT/Setting-1-Frequency-Correct-And-Incorrect-Terms.xlsx")

# How often correct model by BIC in Setting 2 ######################################################
cmBIC <- rdf_bw[rdf_bw$R != "Y1", ]
cmBIC$R <- factor(cmBIC$R)

# BIC (linear) -------------------------------------------------------------------------------------
cmBIC$lm4_C <- gsub("\\(Intercept\\) \\| ", "", cmBIC$lm4_C)
cmBIC$lm4_C <- gsub("low|average|high", "", cmBIC$lm4_C)
cmBIC$lm4_C <- gsub("Sexfemales", "Sex", cmBIC$lm4_C)
cmBIC$lm4_C <- gsub("Diabetesyes", "Diabetes", cmBIC$lm4_C)

# how many times correct?
cmBIC$lm4_CORRECT <- ifelse(cmBIC$lm4_C == "Age | Sex | Age:Sex", 1, 0)

t1 <- as.data.frame.table(table(cmBIC$lm4_CORRECT, cmBIC$S, cmBIC$R))
t1 <- t1[t1$Var1 == 1, 2:4]
t1$Freq <- sprintf("%1.1f%%", t1$Freq/10)
names(t1) <- c("Sample size", "Response", "BIC (linear)")



# BIC (Age + BMI with 2df) -------------------------------------------------------------------------
cmBIC$lm5_C <- gsub("\\(Intercept\\) \\| ", "", cmBIC$lm5_C)
cmBIC$lm5_C <- gsub("low|average|high", "", cmBIC$lm5_C)
cmBIC$lm5_C <- gsub("Sexfemales", "Sex", cmBIC$lm5_C)
cmBIC$lm5_C <- gsub("Diabetesyes", "Diabetes", cmBIC$lm5_C)
cmBIC$lm5_C <- gsub("ns\\(Age, df = 2\\)1|ns\\(Age, df = 2\\)2", "Age", cmBIC$lm5_C)
cmBIC$lm5_C <- gsub("ns\\(BMI, df = 2\\)1|ns\\(BMI, df = 2\\)2", "BMI", cmBIC$lm5_C)
cmBIC$lm5_C <- lapply(str_split(cmBIC$lm5_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
)

# how many times correct?
cmBIC$lm5_CORRECT <- ifelse(cmBIC$lm5_C == "Age | Sex | Age:Sex", 1, 0)

t2 <- as.data.frame.table(table(cmBIC$lm5_CORRECT, cmBIC$S, cmBIC$R))
t2 <- t2[t2$Var1 == 1, 2:4]
t2$Freq <- sprintf("%1.1f%%", t2$Freq/10)

t1$"BIC (rcs, df=2)" <- t2$Freq

# BIC (Age + BMI with 3df) -------------------------------------------------------------------------

cmBIC$lm6_C <- gsub("\\(Intercept\\) \\| ", "", cmBIC$lm6_C)
cmBIC$lm6_C <- gsub("low|average|high", "", cmBIC$lm6_C)
cmBIC$lm6_C <- gsub("Sexfemales", "Sex", cmBIC$lm6_C)
cmBIC$lm6_C <- gsub("Diabetesyes", "Diabetes", cmBIC$lm6_C)
cmBIC$lm6_C <- gsub("ns\\(Age, df = 3\\)1|ns\\(Age, df = 3\\)2|ns\\(Age, df = 3\\)3", "Age", cmBIC$lm6_C)
cmBIC$lm6_C <- gsub("ns\\(BMI, df = 3\\)1|ns\\(BMI, df = 3\\)2|ns\\(BMI, df = 3\\)3", "BMI", cmBIC$lm6_C)
cmBIC$lm6_C <- lapply(str_split(cmBIC$lm6_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
)

# how many times correct?
cmBIC$lm6_CORRECT <- ifelse(cmBIC$lm6_C == "Age | Sex | Age:Sex", 1, 0)

t3 <- as.data.frame.table(table(cmBIC$lm6_CORRECT, cmBIC$S, cmBIC$R))
t3 <- t3[t3$Var1 == 1, 2:4]
t3$Freq <- sprintf("%1.1f%%", t3$Freq/10)

t1$"BIC (rcs, df=3)" <- t3$Freq


# BIC (Age + BMI with 4df) -------------------------------------------------------------------------
cmBIC$lm7_C <- gsub("\\(Intercept\\) \\| ", "", cmBIC$lm7_C)
cmBIC$lm7_C <- gsub("low|average|high", "", cmBIC$lm7_C)
cmBIC$lm7_C <- gsub("Sexfemales", "Sex", cmBIC$lm7_C)
cmBIC$lm7_C <- gsub("Diabetesyes", "Diabetes", cmBIC$lm7_C)
cmBIC$lm7_C <- gsub("ns\\(Age, df = 4\\)1|ns\\(Age, df = 4\\)2|ns\\(Age, df = 4\\)3|ns\\(Age, df = 4\\)4", "Age", cmBIC$lm7_C)
cmBIC$lm7_C <- gsub("ns\\(BMI, df = 4\\)1|ns\\(BMI, df = 4\\)2|ns\\(BMI, df = 4\\)3|ns\\(BMI, df = 4\\)4", "BMI", cmBIC$lm7_C)
cmBIC$lm7_C <- lapply(str_split(cmBIC$lm7_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
)

# how many times correct?
cmBIC$lm7_CORRECT <- ifelse(cmBIC$lm7_C == "Age | Sex | Age:Sex", 1, 0)

t4 <- as.data.frame.table(table(cmBIC$lm7_CORRECT, cmBIC$S, cmBIC$R))
t4 <- t4[t4$Var1 == 1, 2:4]
t4$Freq <- sprintf("%1.1f%%", t4$Freq/10)

t1$"BIC (rcs, df=4)" <- t4$Freq

# 1st and 2nd order effects (discrete) -------------------------------------------------------------
t5 <- as.data.frame.table(table(cmBIC$lm1_C, cmBIC$S, cmBIC$R))
t5 <- t5[t5$Var1 == 5, ]
t5$Freq <- sprintf("%1.1f%%", t5$Freq/10)

t1$"1st + 2nd order (discrete)" <- t5$Freq

# 1st and 2nd order effects (mixed) ----------------------------------------------------------------

t6 <- as.data.frame.table(table(cmBIC$lm2_C, cmBIC$S, cmBIC$R))
t6 <- t6[t6$Var1 == 3, ]
t6$Freq <- sprintf("%1.1f%%", t6$Freq/10)

t1$"1st + 2nd order (mixed)" <- t6$Freq

t1 <- t1[, c(1, 2, 7, 8, 3:6)]
cmall <- t1

# 
# openxlsx::write.xlsx(cmall, 
#                      file = "T:/Projekte/Intersectionality Simulation Study/GandT/Frequency-Age-Sex-AgeSex-Models.xlsx")

# 1st and 2nd order effects (discrete) -------------------------------------------------------------

t1 <- aggregate(cmBIC$lm1_N, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
  })

t1 <- cbind(t1[, 1:2], as.data.frame(t1$x))
names(t1) <- c("Sample size", "Response", "q25", "q50", "q75")

t1$"1st + 2nd order (discrete)" <- paste0(t1$q25, " | ", t1$q50, " | ", t1$q75)


# 1st and 2nd order effects (mixed) ----------------------------------------------------------------

t2 <- aggregate(cmBIC$lm2_N, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
  })

t2 <- cbind(t2[, 1:2], as.data.frame(t2$x))
names(t2) <- c("Sample size", "Response", "q25", "q50", "q75")

t2$Quantiles <- paste0(t2$q25, " | ", t2$q50, " | ", t2$q75)

t1$"1st + 2nd order (mixed)" <- t2$Quantiles


# BIC (linear) -------------------------------------------------------------------------------------
cmBIC$lm4_IC <- cmBIC$lm4_C
cmBIC$lm4_IC <- gsub("\\| ", "", cmBIC$lm4_IC)
cmBIC$lm4_IC <- lapply(str_split(cmBIC$lm4_IC, " "), function(x) {x <- unique(unlist(x))})
cmBIC$lm4_IC <- lapply(cmBIC$lm4_IC, function(x) {x <- setdiff(x, c("Age", "Sex", "Age:Sex"))})
cmBIC$lm4_IC <- unlist(lapply(cmBIC$lm4_IC, length))

t3 <- aggregate(cmBIC$lm4_IC, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
})

t3 <- cbind(t3[, 1:2], as.data.frame(t3$x))
names(t3) <- c("Sample size", "Response", "q25", "q50", "q75")

t3$Quantiles <- paste0(t3$q25, " | ", t3$q50, " | ", t3$q75)

t1$"BIC (linear)" <- t3$Quantiles

# BIC (Age + BMI with 2df) -------------------------------------------------------------------------
cmBIC$lm5_IC <- cmBIC$lm5_C
cmBIC$lm5_IC <- gsub("\\| ", "", cmBIC$lm5_IC)
cmBIC$lm5_IC <- lapply(str_split(cmBIC$lm5_IC, " "), function(x) {x <- unique(unlist(x))})
cmBIC$lm5_IC <- lapply(cmBIC$lm5_IC, function(x) {x <- setdiff(x, c("Age", "Sex", "Age:Sex"))})
cmBIC$lm5_IC <- unlist(lapply(cmBIC$lm5_IC, length))


t4 <- aggregate(cmBIC$lm5_IC, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
})

t4 <- cbind(t4[, 1:2], as.data.frame(t4$x))
names(t4) <- c("Sample size", "Response", "q25", "q50", "q75")

t4$Quantiles <- paste0(t4$q25, " | ", t4$q50, " | ", t4$q75)

t1$"BIC (rcs, df=2)" <- t4$Quantiles


# BIC (Age + BMI with 3df) -------------------------------------------------------------------------

cmBIC$lm6_IC <- cmBIC$lm6_C
cmBIC$lm6_IC <- gsub("\\| ", "", cmBIC$lm6_IC)
cmBIC$lm6_IC <- lapply(str_split(cmBIC$lm6_IC, " "), function(x) {x <- unique(unlist(x))})
cmBIC$lm6_IC <- lapply(cmBIC$lm6_IC, function(x) {x <- setdiff(x, c("Age", "Sex", "Age:Sex"))})
cmBIC$lm6_IC <- unlist(lapply(cmBIC$lm6_IC, length))

t5 <- aggregate(cmBIC$lm6_IC, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
  })

t5 <- cbind(t5[, 1:2], as.data.frame(t5$x))
names(t5) <- c("Sample size", "Response", "q25", "q50", "q75")

t5$Quantiles <- paste0(t5$q25, " | ", t5$q50, " | ", t5$q75)

t1$"BIC (rcs, df=3)" <- t5$Quantiles

# BIC (Age + BMI with 4df) -------------------------------------------------------------------------
cmBIC$lm7_IC <- cmBIC$lm7_C
cmBIC$lm7_IC <- gsub("\\| ", "", cmBIC$lm7_IC)
cmBIC$lm7_IC <- lapply(str_split(cmBIC$lm7_IC, " "), function(x) {x <- unique(unlist(x))})
cmBIC$lm7_IC <- lapply(cmBIC$lm7_IC, function(x) {x <- setdiff(x, c("Age", "Sex", "Age:Sex"))})
cmBIC$lm7_IC <- unlist(lapply(cmBIC$lm7_IC, length))

t6 <- aggregate(cmBIC$lm7_IC, list(cmBIC$S, cmBIC$R), function(x) {
  quantile(x, probs = c(0.25, 0.5, 0.75))
  })

t6 <- cbind(t6[, 1:2], as.data.frame(t6$x))
names(t6) <- c("Sample size", "Response", "q25", "q50", "q75")

t6$Quantiles <- paste0(t6$q25, " | ", t6$q50, " | ", t6$q75)

t1$"BIC (rcs, df=4)" <- t6$Quantiles

t1 <- t1[, c(1:2, 6:11)]

t1[3:8] <- sapply(t1[3:8], function (x) {paste0("(", 
                                                x <- gsub(" \\| ", "; ", x),
                                                ")")
  })

names(t1) <- c("Sample size", "Response", c("Approach 1a",
                                            "Approach 1b",
                                            "Approach 3a",
                                            "Approach 3b",
                                            "Approach 3c",
                                            "Approach 3d"))

# openxlsx::write.xlsx(t1, 
#                      file = "T:/Projekte/Intersectionality Simulation Study/GandT/Frequency-Incorrect-Terms.xlsx")

names(cmall) <- c("Sample size", "Response", c("Approach 1a",
                                               "Approach 1b",
                                               "Approach 3a",
                                               "Approach 3b",
                                               "Approach 3c",
                                               "Approach 3d"))


for (i in names(cmall)[3:8]) {
  cmall[[i]] <- paste0(cmall[[i]], " ", t1[[i]])
}

cmall

# openxlsx::write.xlsx(cmall,
#                      file = "T:/Projekte/Intersectionality Simulation Study/GandT/Setting-2-Frequency-Correct-And-Incorrect-Terms.xlsx")



# Variable selection frequencies in setting 2 ######################################################
vsf <- cmBIC

# terms
tm1 <- c("A", "B", "S", "E", "D", "AB", "AS", "AE", "AD", "BS", "BE", "BD", "SE", "SD", "ED")
tm2 <- c("Age", "BMI", "Sex", "Education", "Diabetes", "Age:BMI", "Age:Sex", "Age:Education", 
         "Age:Diabetes", "BMI:Sex", "BMI:Education", "BMI:Diabetes", "Sex:Education", 
         "Sex:Diabetes", "Education:Diabetes")

# result table
at <- data.frame(Terms = tm1,
                 A1a = NA,
                 A1b = NA,
                 A3a = NA,
                 A3b = NA,
                 A3c = NA,
                 A3d = NA)

# Approach 1a --------------------------------------------------------------------------------------

vsf_a1 <- vsf[, c("I", "R", "S", "lm1_WT")]
names(vsf_a1) <- c("I", "R", "SaS", "lm1_WT")
vsf_a1[, tm1] <- 0

for (i in 1:dim(vsf_a1)[1]) {
  vsf_a1[i, match(unlist(strsplit(vsf_a1$lm1_WT[i], "\\|")), tm1) + 4] <- 1
}

vsf_a1l <- lapply(vsf_a1[, tm1], function(x) {aggregate(x, list(vsf_a1$R, vsf_a1$SaS), sum)})

for (i in tm1) {
  at$A1a[at$Terms == i] <- sprintf("%1.2f%%", vsf_a1l[[i]]$x[vsf_a1l[[i]]$Group.1 == "Y3" & vsf_a1l[[i]]$Group.2 == 1000]/10)
}

# Approach 1b --------------------------------------------------------------------------------------

vsf_a2 <- vsf[, c("I", "R", "S", "lm2_WT")]
names(vsf_a2) <- c("I", "R", "SaS", "lm2_WT")
vsf_a2[, tm1] <- 0

for (i in 1:dim(vsf_a2)[1]) {
  vsf_a2[i, match(unlist(strsplit(vsf_a2$lm2_WT[i], "\\|")), tm1) + 4] <- 1
}

vsf_a2l <- lapply(vsf_a2[, tm1], function(x) {aggregate(x, list(vsf_a2$R, vsf_a2$SaS), sum)})

for (i in tm1) {
  at$A1b[at$Terms == i] <- sprintf("%1.2f%%", vsf_a2l[[i]]$x[vsf_a2l[[i]]$Group.1 == "Y3" & vsf_a2l[[i]]$Group.2 == 1000]/10)
}

# BIC (linear) -------------------------------------------------------------------------------------

vsf_a3a <- vsf[, c("I", "R", "S", "lm4_C")]
names(vsf_a3a) <- c("I", "R", "SaS", "lm4_C")
vsf_a3a[, tm2] <- 0

for (i in 1:dim(vsf_a3a)[1]) {
  vsf_a3a[i, match(unique(trimws(unlist(strsplit(vsf_a3a$lm4_C[i], "\\|")))), tm2) + 4] <- 1
}

vsf_a3al <- lapply(vsf_a3a[, tm2], function(x) {aggregate(x, list(vsf_a3a$R, vsf_a3a$SaS), sum)})

for (i in 1:length(tm1)) {
    at$A3a[at$Terms == tm1[i]] <- sprintf("%1.2f%%", vsf_a3al[[tm2[i]]]$x[vsf_a3al[[tm2[i]]]$Group.1 == "Y3" & vsf_a3al[[tm2[i]]]$Group.2 == 1000]/10)
}


# BIC (Age + BMI with 2df) -------------------------------------------------------------------------

vsf_a3b <- vsf[, c("I", "R", "S", "lm5_C")]
names(vsf_a3b) <- c("I", "R", "SaS", "lm5_C")
vsf_a3b[, tm2] <- 0
vsf_a3b$lm5_C <- as.character(vsf_a3b$lm5_C)
vsf_a3b$lm5_C <- gsub("\\| NA", "", vsf_a3b$lm5_C)

for (i in 1:dim(vsf_a3b)[1]) {
  vsf_a3b[i, match(unique(trimws(unlist(strsplit(vsf_a3b$lm5_C[i], "\\|")))), tm2) + 4] <- 1
}

vsf_a3bl <- lapply(vsf_a3b[, tm2], function(x) {aggregate(x, list(vsf_a3b$R, vsf_a3b$SaS), sum)})

for (i in 1:length(tm1)) {
  at$A3b[at$Terms == tm1[i]] <- sprintf("%1.2f%%", vsf_a3bl[[tm2[i]]]$x[vsf_a3bl[[tm2[i]]]$Group.1 == "Y3" & vsf_a3bl[[tm2[i]]]$Group.2 == 1000]/10)
}


# BIC (Age + BMI with 3df) -------------------------------------------------------------------------

vsf_a3c <- vsf[, c("I", "R", "S", "lm6_C")]
names(vsf_a3c) <- c("I", "R", "SaS", "lm6_C")
vsf_a3c[, tm2] <- 0
vsf_a3c$lm6_C <- as.character(vsf_a3c$lm6_C)
vsf_a3c$lm6_C <- gsub("\\| NA", "", vsf_a3c$lm6_C)

for (i in 1:dim(vsf_a3c)[1]) {
  vsf_a3c[i, match(unique(trimws(unlist(strsplit(vsf_a3c$lm6_C[i], "\\|")))), tm2) + 4] <- 1
}

vsf_a3cl <- lapply(vsf_a3c[, tm2], function(x) {aggregate(x, list(vsf_a3c$R, vsf_a3c$SaS), sum)})

for (i in 1:length(tm1)) {
  at$A3c[at$Terms == tm1[i]] <- sprintf("%1.2f%%", vsf_a3cl[[tm2[i]]]$x[vsf_a3cl[[tm2[i]]]$Group.1 == "Y3" & vsf_a3cl[[tm2[i]]]$Group.2 == 1000]/10)
}

# BIC (Age + BMI with 4df) -------------------------------------------------------------------------

vsf_a3d <- vsf[, c("I", "R", "S", "lm7_C")]
names(vsf_a3d) <- c("I", "R", "SaS", "lm7_C")
vsf_a3d[, tm2] <- 0
vsf_a3d$lm7_C <- as.character(vsf_a3d$lm7_C)
vsf_a3d$lm7_C <- gsub("\\| NA", "", vsf_a3d$lm7_C)

for (i in 1:dim(vsf_a3d)[1]) {
  vsf_a3d[i, match(unique(trimws(unlist(strsplit(vsf_a3d$lm7_C[i], "\\|")))), tm2) + 4] <- 1
}

vsf_a3dl <- lapply(vsf_a3d[, tm2], function(x) {aggregate(x, list(vsf_a3d$R, vsf_a3d$SaS), sum)})

for (i in 1:length(tm1)) {
  at$A3d[at$Terms == tm1[i]] <- sprintf("%1.2f%%", vsf_a3dl[[tm2[i]]]$x[vsf_a3dl[[tm2[i]]]$Group.1 == "Y3" & vsf_a3dl[[tm2[i]]]$Group.2 == 1000]/10)
}


at$Terms <- tm2

openxlsx::write.xlsx(at, file = "T:/Projekte/Intersectionality Simulation Study/GandT/Frequency-All-Terms.xlsx")

# Why is a model with all interaction terms bad in validation data? ################################

p_cs <- ggplot(rdf_bw, aes(x = factor(S), y = lm1_aveCS)) +
  coord_cartesian(ylim = c(0, 6)) +
  geom_boxplot(alpha = 0.5) +
  xlab("Sample size") +
  ylab("Mean absolute coefficient size \n(1st + 2nd order effects (discrete only))") +
  theme_classic(base_size = 14)
p_cs

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f2-Y1-coefficient-size-M1.png",
       dpi = 600,
       width = 4,
       height = 6)


# Plot MSE #########################################################################################

rdfl <- melt(setDT(rdf_bw),
             measure.vars = patterns("MSE"),
             id.vars = c("I", "R", "S"),
             variable.name = "Model",
             value.name = "MSE")

# how many models could not be predicted?
table(rdfl$Model[grep("[:alpha:]", rdfl$MSE)], 
      rdfl$S[grep("[:alpha:]", rdfl$MSE)], 
      rdfl$R[grep("[:alpha:]", rdfl$MSE)])

# remove alpha-numeric
rdfl$MSE <- as.numeric(rdfl$MSE)

rdfl$Data <- grepl("_ld", rdfl$Model)
rdfl$Data <- factor(ifelse(rdfl$Data == TRUE, "Learning data", "Validation data"),
                    levels = c("Learning data", "Validation data"))

rdfl$Model <- gsub("\\_.*", "", rdfl$Model)
rdfl$Model <- factor(rdfl$Model, 
                     levels = unique(rdfl$Model),
                     label = c("Approach \n1a",
                               "Approach \n1b",
                               "Approach \n2",
                               "Approach \n3a",
                               "Approach \n3b",
                               "Approach \n3c",
                               "Approach \n3d"))

# get references for hline
rb <- aggregate(rdfl$MSE, list(rdfl$Data, rdfl$R, rdfl$S, rdfl$Model), 
                function(x) {median(x, na.rm = TRUE)})
openxlsx::write.xlsx(rb, 
                     file = "T:/Projekte/Intersectionality Simulation Study/GandT/MSE-Aggregated.xlsx")


rl <- rb$x[rb$Group.1 == "Validation data" & rb$Group.2 == "Y1" & rb$Group.4 == "BE-BIC \n(linear)"]


pY1 <- ggplot(rdfl[rdfl$R == "Y1", ], 
              aes(x = factor(S), y = MSE, fill = Data), alpha = 0.3) + 
  facet_grid(Model ~ ., scales = "free", space = "free") +
  geom_hline(yintercept = rl, color = "gray", linetype = "dashed") +
  scale_y_continuous(breaks = 1:4, trans = "log10") +
  geom_boxplot(alpha = 0.5) +
  ylab("Mean squared error") +
  xlab("Sample size") +
  scale_fill_jco() +
  scale_color_jco() +
  theme_classic(base_size = 14) +
  guides(color = guide_legend(nrow = 2, byrow = TRUE)) +
  coord_flip(ylim = c(1, 4.5)) +
  theme(legend.position = c(0.8, 0.12),
        legend.title = element_blank())
pY1

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f3-MSE-Y1-results.png",
       dpi = 600,
       width = 8,
       height = 14)

rl <- rb$x[rb$Group.1 == "Validation data" & rb$Group.2 == "Y2" & rb$Group.4 == "BE-BIC \n(linear)"]

pY2 <- ggplot(rdfl[rdfl$R == "Y2", ], 
              aes(x = factor(S), y = MSE, fill = Data), alpha = 0.3) + 
  facet_grid(Model ~ ., scales = "free", space = "free") +
  geom_hline(yintercept = rl, color = "gray", linetype = "dashed") +
  #scale_y_continuous(breaks = 1:4, trans = "log10") +
  geom_boxplot(alpha = 0.5) +
  ylab("Mean squared error") +
  xlab("Sample size") +
  scale_fill_jco() +
  scale_color_jco() +
  theme_classic(base_size = 14) +
  guides(color = guide_legend(nrow = 2, byrow = TRUE)) +
  coord_flip(ylim = c(0.3, 3)) +
  theme(legend.position = c(0.8, 0.12),
        legend.title = element_blank())
pY2

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f4-MSE-Y2-results.png",
       dpi = 600,
       width = 8,
       height = 14)


rl <- rb$x[rb$Group.1 == "Validation data" & rb$Group.2 == "Y3" & rb$Group.4 == "BE-BIC \n(linear)"]

pY3 <- ggplot(rdfl[rdfl$R == "Y3", ], 
              aes(x = factor(S), y = MSE, fill = Data), alpha = 0.3) + 
  facet_grid(Model ~ ., scales = "free", space = "free") +
  geom_hline(yintercept = rl, color = "gray", linetype = "dashed") +
  geom_boxplot(alpha = 0.5) +
  ylab("Mean squared error") +
  xlab("Sample size") +
  scale_fill_jco() +
  scale_color_jco() +
  theme_classic(base_size = 14) +
  guides(color = guide_legend(nrow = 2, byrow = TRUE)) +
  coord_flip(ylim = c(0, 2.5)) +
  theme(legend.position = c(0.8, 0.12),
        legend.title = element_blank())
pY3

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f5-MSE-Y3-results.png",
       dpi = 600,
       width = 8,
       height = 14)

# -> graphical abstract ############################################################################

tmp <- rdfl[rdfl$R == "Y3" & 
              rdfl$Model %in% c("Approach \n1a", "Approach \n2", "Approach \n3c") &
              rdfl$S %in% c(200, 1000, 3000), ]

tmp$Model <- as.character(tmp$Model)
tmp$Model <- factor(tmp$Model, labels = c("Approach 1", "Approach 2", "Approach 3"))
unique(tmp$Model)
pGA <- ggplot(tmp, 
              aes(x = factor(S), y = MSE, fill = Data), alpha = 0.3) + 
  facet_grid(. ~ Model, scales = "free", space = "free") +
  coord_cartesian(ylim = c(0.3, 3)) +
  geom_hline(yintercept = seq(0.6, 2, 0.2), color = "gray", linetype = "dashed") +
  geom_boxplot(alpha = 0.5) +
  ylab("Mean squared error") +
  xlab("Sample size") +
  scale_fill_jco() +
  scale_color_jco() +
  theme_classic(base_size = 18) +
  guides(color = guide_legend(nrow = 2, byrow = TRUE)) +
  theme(legend.position = "bottom",
        legend.title = element_blank(),
        rect = element_rect(fill = "transparent"),
        panel.background = element_rect(fill = "transparent",
                                        colour = NA_character_),
        strip.background =element_rect(fill = "transparent"))
pGA

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/GA-MSE-Y3-results.png",
       dpi = 600,
       width = 5.5,
       height = 8,
       bg = "transparent")

####################################################################################################

rl <- rb$x[rb$Group.1 == "Validation data" & rb$Group.2 == "Y4" & rb$Group.4 == "BE-BIC \n(linear)"]

pY4 <- ggplot(rdfl[rdfl$R == "Y4", ], 
              aes(x = factor(S), y = MSE, fill = Data), alpha = 0.3) + 
  facet_grid(Model ~ ., scales = "free", space = "free") +
  geom_hline(yintercept = rl, color = "gray", linetype = "dashed") +
  geom_boxplot(alpha = 0.5) +
  ylab("Mean squared error") +
  xlab("Sample size") +
  scale_fill_jco() +
  scale_color_jco() +
  theme_classic(base_size = 14) +
  guides(color = guide_legend(nrow = 2, byrow = TRUE)) +
  coord_flip(ylim = c(0, 1)) +
  theme(legend.position = c(0.8, 0.12),
        legend.title = element_blank())
pY4



ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f6-MSE-Y4-results.png",
       dpi = 600,
       width = 8,
       height = 14)


# Coefficient selection by stratification approach -------------------------------------------------

snames <- c('Strata_ASE[-Inf,48) | females | average', 'Strata_ASE[-Inf,48) | females | high',
            'Strata_ASE[-Inf,48) | females | low', 'Strata_ASE[-Inf,48) | females | none',
            'Strata_ASE[-Inf,48) | males | average', 'Strata_ASE[-Inf,48) | males | high',
            'Strata_ASE[-Inf,48) | males | low', 'Strata_ASE[-Inf,48) | males | none',
            'Strata_ASE[48,55) | females | average', 'Strata_ASE[48,55) | females | high',
            'Strata_ASE[48,55) | females | low', 'Strata_ASE[48,55) | females | none',
            'Strata_ASE[48,55) | males | high', 'Strata_ASE[48,55) | males | low',
            'Strata_ASE[48,55) | males | none', 'Strata_ASE[55,62) | females | average',
            'Strata_ASE[55,62) | females | high', 'Strata_ASE[55,62) | females | low',
            'Strata_ASE[55,62) | females | none', 'Strata_ASE[55,62) | males | average',
            'Strata_ASE[55,62) | males | high', 'Strata_ASE[55,62) | males | low',
            'Strata_ASE[55,62) | males | none', 'Strata_ASE[62, Inf] | females | average',
            'Strata_ASE[62, Inf] | females | high', 'Strata_ASE[62, Inf] | females | low',
            'Strata_ASE[62, Inf] | females | none', 'Strata_ASE[62, Inf] | males | average',
            'Strata_ASE[62, Inf] | males | high', 'Strata_ASE[62, Inf] | males | low',
            'Strata_ASE[62, Inf] | males | none')

cssa <- rdf_bw[, c("S", "R", paste0("S", 1:31))]

cssal <- melt(setDT(cssa),
              id.vars = c("S", "R"),
              measure = paste0("S", 1:31),
              value.name = "Selected",
              variable.name = "Stratum")

cssal$Stratum <- factor(cssal$Stratum)

cssaa <- aggregate(cssal$Selected, list(cssal$S, cssal$R, cssal$Stratum), function(x) {sum(x, na.rm = TRUE)/10})

names(cssaa) <- c("Sample_size", "Response", "Strata", "Frequency")

cssaa$Strata <- factor(cssaa$Strata, 
                       levels = paste0("S", 1:31),
                       labels = snames)

ggplot(cssaa, aes(x = Strata, y = Frequency, fill = factor(Sample_size))) +
  facet_grid(. ~ Response) +
  geom_bar(stat = "identity", position = position_dodge()) +
  geom_vline(xintercept = 13.5, color = "gray", lty = "dashed") +
  coord_flip() +
  scale_fill_jco() +
  theme_classic() +
  theme(legend.position = c(0.15, 0.7)) +
  guides(fill = guide_legend(title="Sample size"))

ggsave(last_plot(), 
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f7-Strata-Significance.png",
       dpi = 600,
       width = 8,
       height = 12)

# Best Model of BE-BIC -----------------------------------------------------------------------------

bestBIC <- rdf_bw[rdf_bw$R == "Y3" & rdf_bw$S == 1000, ]

# BIC (linear) 
bestBIC$lm4_C <- gsub("\\(Intercept\\) \\| ", "", bestBIC$lm4_C)
bestBIC$lm4_C <- gsub("low|average|high", "", bestBIC$lm4_C)
bestBIC$lm4_C <- gsub("Sexfemales", "Sex", bestBIC$lm4_C)
bestBIC$lm4_C <- gsub("Diabetesyes", "Diabetes", bestBIC$lm4_C)
bestBIC$lm4_C <- unlist(lapply(str_split(bestBIC$lm4_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
))

BIClin <- as.data.frame(table(bestBIC$lm4_C))

# BIC (Age + BMI with 2df) 
bestBIC$lm5_C <- gsub("\\(Intercept\\) \\| ", "", bestBIC$lm5_C)
bestBIC$lm5_C <- gsub("low|average|high", "", bestBIC$lm5_C)
bestBIC$lm5_C <- gsub("Sexfemales", "Sex", bestBIC$lm5_C)
bestBIC$lm5_C <- gsub("Diabetesyes", "Diabetes", bestBIC$lm5_C)
bestBIC$lm5_C <- gsub("ns\\(Age, df = 2\\)1|ns\\(Age, df = 2\\)2", "Age", bestBIC$lm5_C)
bestBIC$lm5_C <- gsub("ns\\(BMI, df = 2\\)1|ns\\(BMI, df = 2\\)2", "BMI", bestBIC$lm5_C)
bestBIC$lm5_C <- unlist(lapply(str_split(bestBIC$lm5_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
))

BIC2df <- as.data.frame(table(bestBIC$lm5_C))

# BIC (Age + BMI with 3df) 
bestBIC$lm6_C <- gsub("\\(Intercept\\) \\| ", "", bestBIC$lm6_C)
bestBIC$lm6_C <- gsub("low|average|high", "", bestBIC$lm6_C)
bestBIC$lm6_C <- gsub("Sexfemales", "Sex", bestBIC$lm6_C)
bestBIC$lm6_C <- gsub("Diabetesyes", "Diabetes", bestBIC$lm6_C)
bestBIC$lm6_C <- gsub("ns\\(Age, df = 3\\)1|ns\\(Age, df = 3\\)2|ns\\(Age, df = 3\\)3", "Age", bestBIC$lm6_C)
bestBIC$lm6_C <- gsub("ns\\(BMI, df = 3\\)1|ns\\(BMI, df = 3\\)2|ns\\(BMI, df = 3\\)3", "BMI", bestBIC$lm6_C)
bestBIC$lm6_C <- unlist(lapply(str_split(bestBIC$lm6_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
))

BIC3df <- as.data.frame(table(bestBIC$lm6_C))


# BIC (Age + BMI with 4df) 
bestBIC$lm7_C <- gsub("\\(Intercept\\) \\| ", "", bestBIC$lm7_C)
bestBIC$lm7_C <- gsub("low|average|high", "", bestBIC$lm7_C)
bestBIC$lm7_C <- gsub("Sexfemales", "Sex", bestBIC$lm7_C)
bestBIC$lm7_C <- gsub("Diabetesyes", "Diabetes", bestBIC$lm7_C)
bestBIC$lm7_C <- gsub("ns\\(Age, df = 4\\)1|ns\\(Age, df = 4\\)2|ns\\(Age, df = 4\\)3|ns\\(Age, df = 4\\)4", "Age", bestBIC$lm7_C)
bestBIC$lm7_C <- gsub("ns\\(BMI, df = 4\\)1|ns\\(BMI, df = 4\\)2|ns\\(BMI, df = 4\\)3|ns\\(BMI, df = 4\\)4", "BMI", bestBIC$lm7_C)
bestBIC$lm7_C <- unlist(lapply(str_split(bestBIC$lm7_C, " | "), function(x) {
  x <- gsub("\\|", NA, x) 
  x <- paste0(unique(x), collapse = " | ")
  x <- gsub("NA \\| ", "", x) 
}
))

BIC4df <- as.data.frame(table(bestBIC$lm7_C))

BIClin$Approach <- "BE-BIC (linear)"
BIC2df$Approach <- "BE-BIC (rcs, df=2)"
BIC3df$Approach <- "BE-BIC (rcs, df=3)"
BIC4df$Approach <- "BE-BIC (rcs, df=4)"

# only best five
BIClin <- BIClin[order(BIClin$Freq, decreasing = TRUE), ]
BIC2df <- BIC2df[order(BIC2df$Freq, decreasing = TRUE), ]
BIC3df <- BIC3df[order(BIC3df$Freq, decreasing = TRUE), ]
BIC4df <- BIC4df[order(BIC4df$Freq, decreasing = TRUE), ]

BIClin <- BIClin[1:3, ]
BIC2df <- BIC2df[1:3, ]
BIC3df <- BIC3df[1:3, ]
BIC4df <- BIC4df[1:3, ]

# Merge MSE from internal validation data
t1 <- aggregate(bestBIC$lm4_vdMSE[bestBIC$lm4_C %in% BIClin$Var1], 
                list(bestBIC$lm4_C[bestBIC$lm4_C %in% BIClin$Var1]),
                mean)
t2 <- aggregate(bestBIC$lm5_vdMSE[bestBIC$lm5_C %in% BIC2df$Var1], 
                list(bestBIC$lm5_C[bestBIC$lm5_C %in% BIC2df$Var1]),
                mean)
t3 <- aggregate(bestBIC$lm6_vdMSE[bestBIC$lm6_C %in% BIC3df$Var1], 
                list(bestBIC$lm6_C[bestBIC$lm6_C %in% BIC3df$Var1]),
                mean)
t4 <- aggregate(bestBIC$lm7_vdMSE[bestBIC$lm7_C %in% BIC4df$Var1], 
                list(bestBIC$lm7_C[bestBIC$lm7_C %in% BIC4df$Var1]),
                mean)

BIClin <- merge(BIClin, t1, by.x = "Var1", by.y = "Group.1")
BIC2df <- merge(BIC2df, t2, by.x = "Var1", by.y = "Group.1")
BIC3df <- merge(BIC3df, t3, by.x = "Var1", by.y = "Group.1")
BIC4df <- merge(BIC4df, t4, by.x = "Var1", by.y = "Group.1")

BIClin <- BIClin[order(BIClin$Freq, decreasing = TRUE), ]
BIC2df <- BIC2df[order(BIC2df$Freq, decreasing = TRUE), ]
BIC3df <- BIC3df[order(BIC3df$Freq, decreasing = TRUE), ]
BIC4df <- BIC4df[order(BIC4df$Freq, decreasing = TRUE), ]


BICbest <- rbind(BIClin, BIC2df, BIC3df, BIC4df)
names(BICbest) <- c("Terms", "Freq", "Model", "MSE")
BICbest$Freq <- sprintf("%1.2f%%", BICbest$Freq/10)
BICbest


openxlsx::write.xlsx(BICbest, 
                     file = "T:/Projekte/Intersectionality Simulation Study/GandT/TOP-3-Models-BE-BIC-And-MSE.xlsx")

# why approach 1 most often fails ------------------------------------------------------------------

source("T:/Projekte/Intersectionality Simulation Study/Z-01-utility-functions.R")

# create data
Ns <- 1000
nvar <- 6
sigma1 <- matrix(rep(0, nvar^2), nrow = nvar)
diag(sigma1) <- c(100, 25, 1, 1, 1, 2)
means <- c(55, 25, rep(0, nvar - 3), 5)

set.seed(1)
z1 <- mvrnorm(Ns, mu = means, Sigma = sigma1, empirical = TRUE)
ds1 <- util_prep_df(z1)
ds1 <- util_to_factors(ds1)
ds1$l1 <- "Predicted by \ncategorization"
ds1$l2 <- "Predicted by RCS"

# fit model with correct terms
lm1 <- lm(Y4 ~ AgeG + Sex + AgeG:Sex, data = ds1)

# True effect
agesc <- as.numeric(scale(ds1$Age, scale = FALSE))
ds1$TE <- 5 + 0.05 * agesc + 0.3*ifelse(ds1$Sex == "females", 1, 0) + 
  0.008 * ifelse(agesc > 0 & ds1$Sex ==  "females", 1, 0) * agesc^2

# get predicetd values
ds1$PredictedCAT <- predict(lm1, type = "response")

p1 <- ggplot(ds1, aes(x = Age, y = Y4, group = Sex, color = Sex, label = l1)) +
  geom_point(alpha = 0.3) +
  coord_cartesian(ylim = c(2, 10)) +
  geom_labelline(data = ds1, aes(x = Age, y = PredictedCAT), hjust = 0.9) +
  geom_line(data = ds1, aes(x = Age, y = TE, linetype = Sex), color = "black", alpha = 0.5) +
  scale_color_manual(values = c('#d8b365','#5ab4ac')) +
  xlab("") +
  theme_classic(base_size = 12) +
  ggtitle("Non-linear change in females above middle age (intersection)") +
  theme(legend.position = "none")

# fitted using RCS
lm2 <- lm(Y4 ~ ns(Age, df = 3) + Sex + ns(Age, df = 3):Sex, data = ds1)
ds1$PredictedRCS <- predict(lm2, type = "response")

p2 <- ggplot(ds1, aes(x = Age, y = Y4, group = Sex, color = Sex, label = l2)) +
  geom_point(alpha = 0.3) +
  coord_cartesian(ylim = c(2, 10)) +
  geom_labelline(data = ds1, aes(x = Age, y = PredictedRCS), hjust = 0.7, linewidth = 1, text_only = TRUE) +
  geom_line(data = ds1, aes(x = Age, y = TE, linetype = Sex), color = "black", alpha = 0.5) +
  scale_color_manual(values = c('#d8b365','#5ab4ac')) +
  xlab("") +
  theme_classic(base_size = 12) +
  ggtitle("Non-linear change in females above middle age (intersection)") +
  theme(legend.position = "none")
p2


# also with true linear interactions but varying slope
ds1$Y5 <- 5 + 0.05*agesc + 0.3*ifelse(ds1$Sex == "females", 1, 0) + 
  0.08*ifelse(agesc > 0 & ds1$Sex == "females", 1, 0)*agesc + rnorm(Ns, 0, 0.25)
ds1$TE5 <- 5 + 0.05*agesc + 0.3*ifelse(ds1$Sex == "females", 1, 0) + 
  0.08*ifelse(agesc > 0 & ds1$Sex == "females", 1, 0)*agesc

lm3 <- lm(Y5 ~ AgeG + Sex + AgeG:Sex, data = ds1)

ds1$PredictedLIN <- predict(lm3, type = "response")


p3 <- ggplot(ds1, aes(x = Age, y = Y5, group = Sex, color = Sex, label = l1)) +
  geom_point(alpha = 0.3) +
  coord_cartesian(ylim = c(2, 10)) +
  geom_labelline(data = ds1, aes(x = Age, y = PredictedLIN), hjust = 0.9) +
  geom_line(data = ds1, aes(x = Age, y = TE5, linetype = Sex), color = "black", alpha = 0.5) +
  scale_color_manual(values = c('#d8b365','#5ab4ac')) +
  theme_classic(base_size = 12) +
  ggtitle("Linear change in females above middle age (intersection)") +
  theme(legend.position = c(0.2, 0.8))
p3

cowplot::plot_grid(p1, p2, p3, ncol = 1, nrow = 3)

ggsave(last_plot(),
       file = "T:/Projekte/Intersectionality Simulation Study/GandT/f8-approach-1-piecewise-constant.png",
       dpi = 600,
       height = 12,
       width = 8)


# EOF ----------------------------------------------------------------------------------------------
